#!/bin/sh
#@# OS: Ubuntu 16.04 xenial

# -e : Exit immediately if a command exits with a non-zero status.
set -e

# Need root privileges
if ! [ "$(whoami)" = "root" ]; then
    printf '%b' "\\033[1;31m ! You have to run this script with root privileges ! \\033[0m\\n";
    exit 40;
fi

# Create WorkDir
mkdir "$CARTODB_INSTALL_WORKDIR"
chown root:sudo "$CARTODB_INSTALL_WORKDIR"
chmod 775 "$CARTODB_INSTALL_WORKDIR"
