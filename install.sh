#!/bin/sh
#@# Following :
#@#   - http://cartodb.readthedocs.io/en/latest/install.html
#@#   - https://gist.github.com/azvoleff/f8f06d22a8a4d89401e09d6607a5ecc4
#@#   - https://github.com/CartoDB/Windshaft-cartodb/blob/master/INSTALL.md
#@#   - https://github.com/CartoDB/CartoDB-SQL-API
#@#   - https://www.phusionpassenger.com/library/install/nginx/install/oss/xenial/
#@# OS: Ubuntu 16.04 xenial

# -e : Exit immediately if a command exits with a non-zero status.
set -e

# VARIABLES
export CARTODB_INSTALL_WORKDIR="/srv/cartodb"
export RUBY_PATH="/opt/rubies/ruby-2.2.3/bin"
export CARTODB_ENVIRONMENT="production"

# Include functions.inc
# shellcheck source=functions.inc.sh
. ./functions.inc.sh

## Do not run this script as root
check_user

# RUN init.sh
# shellcheck source=init.sh
sudo ./init.sh

# System locales
## I do nothing because default locale is en_US.UTF-8 ...

# Update&Upgrade packages
sys_upgrade

# APT tools
sys_install python-software-properties

# Build essentials & GIT
sys_install autoconf binutils-doc bison build-essential flex \
    gcc-4.9 g++-4.9 \
    git
export CC=/usr/bin/gcc-4.9
export CXX=/usr/bin/g++-4.9

# PostgreSQL
## Add postgresql repository
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sys_update
## Install client & server packages
sys_install libpq5 libpq-dev postgresql-client-9.6 postgresql-client-common \
    postgresql-9.6 postgresql-contrib-9.6 postgresql-server-dev-9.6 postgresql-plpython-9.6

## Allow connections with postgres user from localhost without authentication
### Make backup of pg_hba.conf
PG_hba_file="/etc/postgresql/9.6/main/pg_hba.conf"
su_backup_file "${PG_hba_file}"
### Modify conf with sed
sed -i 's/\(peer\|md5\)/trust/' "${PG_hba_file}"
### For these changes to take effect, you’ll need to restart postgres
service_restart postgresql

## Create some users in PostgreSQL. These users are used by some CartoDB apps internally
sudo createuser publicuser --no-createdb --no-superuser -U postgres
sudo createuser tileuser --no-createrole --no-createdb --no-superuser -U postgres

## Install CartoDB postgresql extension
cd "$CARTODB_INSTALL_WORKDIR"
git clone https://github.com/CartoDB/cartodb-postgresql.git
cd cartodb-postgresql
### git checkout <LATEST cartodb-postgresql tag>
git fetch --tags
# shellcheck disable=SC2006,SC2046
latestTag=$(git describe --tags `git rev-list --tags --max-count=1`)
git checkout "$latestTag"
### install with make
sudo make all install

# GIS dependencies
## Add GIS PPA
add-apt-repository -y ppa:ubuntugis/ppa && apt update
## Install Proj, JSON, GEOS
sys_install proj-bin proj-data libproj-dev \
    libjson0 libjson0-dev python-simplejson \
    libgeos-c1v5 libgeos-dev
## Install GDAL
sys_install gdal-bin libgdal1-dev libgdal-dev
###?? Build gdal 2.1 ??
#? sys_install python-dev python-gdal python3-gdal
#? apt build-dep python-gdal
#? apt build-dep gdal
#? wget http://download.osgeo.org/gdal/2.1.0/gdal-2.1.0.tar.gz
#? tar xzf gdal-2.1.0.tar.gz && rm -v gdal-2.1.0.tar.gz
#? cd gdal-2.1.0/
#? ./configure --prefix=/usr/
#? make
#? make install
#? cd swig/python/
#? python setup.py install


# PostGIS
## Install PostGIS
sys_install libxml2-dev liblwgeom-2.2-5 postgis postgresql-9.6-postgis-2.2 postgresql-9.6-postgis-scripts
## Initialize template postgis database.
sudo createdb -T template0 -O postgres -U postgres -E UTF8 template_postgis
sudo createlang plpgsql -U postgres -d template_postgis
psql -U postgres template_postgis -c 'CREATE EXTENSION postgis;CREATE EXTENSION postgis_topology;'
sudo ldconfig
## Restart PostgreSQL after all this process
service_restart postgresql
## Run an installcheck to verify the database has been installed properly
cd "$CARTODB_INSTALL_WORKDIR/cartodb-postgresql/"
set +e
sudo PGUSER=postgres make installcheck
set -e
cd "$CARTODB_INSTALL_WORKDIR"

# REDIS
sys_install redis-server
## Configure Redis persistance
### Make backup of redis.conf
REDIS_conf_file="/etc/redis/redis.conf"
su_backup_file "${REDIS_conf_file}"
### Modify conf with sed
sudo sed -i '/^save 60 10000;*/a save ""' "${REDIS_conf_file}"
sudo sed -i "s/^appendonly .*\\?/appendonly yes/" "${REDIS_conf_file}"
sudo sed -i "s/^appendfsync .*\\?/appendfsync everysec/" "${REDIS_conf_file}"
### For these changes to take effect, you’ll need to restart redis-server
service_restart redis-server

# NODEJS
## Install NodeJS
cd "$HOME"
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
export NVM_DIR="$HOME/.nvm"
# shellcheck disable=SC1090
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
nvm install 6.9.2
### If npm version is not 3.10.9 you should update it:
if [ ! "$(npm -v)" = "3.10.9" ]; then npm install npm@3.10.9 -g; fi
## We will also install some development libraries that will be necessary to build some Node modules:
sys_install libpixman-1-0 libpixman-1-dev \
    libcairo2-dev libjpeg-dev libgif-dev libpango1.0-dev
## Lastly, the below is necessary for Phusion passenger to be able to find node
sudo ln -sf "$NVM_DIR/versions/node/v6.9.2/bin/node" /usr/local/bin/node

# SQL API
## Download API
cd "$CARTODB_INSTALL_WORKDIR"
git clone git://github.com/CartoDB/CartoDB-SQL-API.git
cd CartoDB-SQL-API
git checkout master
## Install npm dependencies
npm install
## Create configuration. The name of the filename of the configuration must be the same than the environment you are going to use to start the service.
cp config/environments/${CARTODB_ENVIRONMENT}.js.example config/environments/${CARTODB_ENVIRONMENT}.js
mkdir logs/
# Start the service. The second parameter is always the environment if the service. Remember to use the same you used in the configuration.
#! -> node app.js ${CARTODB_ENVIRONMENT} <----<----<----<----<----<----<----<----<----<----<----<----<----<----<----<----

# MAPS API
## Download API
cd "$CARTODB_INSTALL_WORKDIR"
git clone git://github.com/CartoDB/Windshaft-cartodb.git
cd Windshaft-cartodb
git checkout master
## Install npm dependencies
npm install -g yarn@0.28.4
yarn && npm install
## Create configuration. The name of the filename of the configuration must be the same than the environment you are going to use to start the service.
cp config/environments/${CARTODB_ENVIRONMENT}.js.example config/environments/${CARTODB_ENVIRONMENT}.js
# Start the service. The second parameter is always the environment if the service. Remember to use the same you used in the configuration.
#! -> node app.js ${CARTODB_ENVIRONMENT} <----<----<----<----<----<----<----<----<----<----<----<----<----<----<----<----

# RUBY
## Download ruby-install. Ruby-install is a script that makes ruby install easier. It's not needed to get ruby installed but it helps in the process.
cd "$CARTODB_INSTALL_WORKDIR"
wget -O ruby-install-0.5.0.tar.gz https://github.com/postmodern/ruby-install/archive/v0.5.0.tar.gz
tar -xzf ruby-install-0.5.0.tar.gz
cd ruby-install-0.5.0/
sudo make install
## Install some ruby dependencies
sys_install libreadline6-dev openssl
## Install ruby 2.2.3. CartoDB has been deeply tested with Ruby 2.2.
sudo ruby-install ruby 2.2.3
sudo chown -R "$USER":"$USER" /opt/rubies/ruby-2.2.3/
## Ruby-install will leave everything in /opt/rubies/ruby-2.2.3/bin. To be able to run ruby and gem later on, you'll need to add the Ruby 2.2.3 bin folder to your PATH variable.
export PATH=${RUBY_PATH}:${PATH}
export RAILS_ENV=${CARTODB_ENVIRONMENT}
# shellcheck disable=SC2016
(
    printf '%b' "PATH=${RUBY_PATH}:\$PATH\\n" >> "${HOME}/.bashrc"
    printf '%b' "RAILS_ENV=${CARTODB_ENVIRONMENT}\\n" >> "${HOME}/.bashrc"
    printf '%b' "PATH=${RUBY_PATH}:\$PATH\\n" | sudo tee -a "/root/.bashrc" >/dev/null
)
## Install bundler. Bundler is an app used to manage ruby dependencies. It is needed by CartoDB's editor
gem install bundler
## Install compass. It will be needed later on by CartoDB's editor
gem install compass

# EDITOR
## Download the editor code
cd "$CARTODB_INSTALL_WORKDIR"
git clone --recursive https://github.com/CartoDB/cartodb.git
cd cartodb
## Install pip
wget  -O /tmp/get-pip.py https://bootstrap.pypa.io/get-pip.py
sudo -H python /tmp/get-pip.py
## Install a necessary package for python dependencies & Install dependencies
sys_install python-all-dev \
    imagemagick unp zip
RAILS_ENV=${CARTODB_ENVIRONMENT} bundle install
npm install
### Fixing bad GDAL version constraint in python_requirements.txt
cp -v python_requirements.txt python_requirements_fix.txt
sed -i 's/^gdal=.*/gdal==2.1.0/' python_requirements_fix.txt
### Install python deps
sudo -H pip install --no-binary :all: -r python_requirements_fix.txt
rm -v python_requirements_fix.txt
## Add the grunt command to the PATH
export PATH=$PATH:$PWD/node_modules/grunt-cli/bin
## Install all necesary gems
bundle install
## Install grunt-cli
npm install -g grunt-cli
cp config/environments/${CARTODB_ENVIRONMENT}.js.example config/environments/${CARTODB_ENVIRONMENT}.js
### Create ${CARTODB_ENVIRONMENT} configuration.
cp config/grunt_${CARTODB_ENVIRONMENT}.json.sample config/grunt_${CARTODB_ENVIRONMENT}.json
cp config/grunt_${CARTODB_ENVIRONMENT}.json.sample config/grunt_true.json
## Precompile assets. Note that the last parameter is the environment used to run the application. It must be the same used in the Maps and SQL APIs
bundle exec grunt --environment=${CARTODB_ENVIRONMENT}
## Create configuration files
cp config/app_config.yml.sample config/app_config.yml
cp config/database.yml.sample config/database.yml
## Initialize the metadata database
RAILS_ENV=${CARTODB_ENVIRONMENT} bundle exec rake db:create
RAILS_ENV=${CARTODB_ENVIRONMENT} bundle exec rake db:migrate


# ----------------------------
# **PRODUCTION CARTODB SETUP**
# ----------------------------
if [ "$CARTODB_ENVIRONMENT" = "production" ]; then
    cd "$CARTODB_INSTALL_WORKDIR"

# NGINX + PHUSION PASSENGER
#
## Install Passenger packages
### Install our PGP key and add HTTPS support for APT
    sys_install dirmngr gnupg apt-transport-https ca-certificates
    sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
### Add our APT repository
    sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger xenial main > /etc/apt/sources.list.d/passenger.list'
    sys_update
### Install Passenger + Nginx
    sudo apt-get install -y nginx nginx-extras passenger
## Enable the Passenger Nginx module and restart Nginx
    su_backup_file /etc/nginx/nginx.conf
    sudo sed -i 's!# include /etc/nginx/passenger.conf;!include /etc/nginx/passenger.conf;!' /etc/nginx/nginx.conf
    service_restart nginx
## Check installation
    sudo passenger-config validate-install --auto
## Install passenger gem
    gem install passenger
## Setup nginx.conf under /etc/nginx/conf.d, and copy over SSL .pem and .key files to /etc/ssl/
#????? ...

# SETUP VARNISH
    sys_install varnish
fi
