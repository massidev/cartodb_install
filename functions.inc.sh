#!/bin/sh
#@# OS: Ubuntu 16.04 xenial

check_user()
{
    # Do not run this script as root
    if [ "$(whoami)" = "root" ]; then
        printf '%b' "\\033[1;31m ! Don't run this script as root ! \\033[0m\\n";
        exit 40;
    fi

    printf '%b' "\\033[1;32m Good ! You're running this script as $(whoami) \\033[0m\\n";
}

sys_install()
{
    sudo apt install -y "$@"
}

sys_update()
{
    sudo apt update
}

sys_upgrade()
{
    sudo apt update && sudo apt upgrade -y
}

su_backup_file()
{
    filename="$(basename "$1")"
    sudo cp -v "$1" "${CARTODB_INSTALL_WORKDIR}/${filename}.bkp"
}

service_restart()
{
    sudo "/etc/init.d/${1}" restart
}
