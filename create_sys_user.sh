#!/bin/sh
#@# OS: Ubuntu 16.04 xenial

# -e : Exit immediately if a command exits with a non-zero status.
set -e

# Need root privileges
if ! [ "$(whoami)" = "root" ]; then
    printf '%b' "\\033[1;31m ! You have to run this script with root privileges ! \\033[0m\\n";
    exit 40;
fi

# Create CertoDB user with sudo privileges (default name : cartoman)
CartoUser="cartoman"
useradd -Um -G sudo,adm -s /bin/bash -c CartoMan "$CartoUser"
# User have to run sudo without password
CartoSudoersFile="100-carto-user"
printf '%s ALL=(ALL) NOPASSWD:ALL' "$CartoUser" > "$CartoSudoersFile"
visudo -c -q -f "$CartoSudoersFile" && \
    chown root:root "$CartoSudoersFile" && \
    chmod 440 "$CartoSudoersFile" && \
    mv "$CartoSudoersFile" /etc/sudoers.d/


